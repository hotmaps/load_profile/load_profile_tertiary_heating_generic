[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687239.svg)](https://doi.org/10.5281/zenodo.4687239)

# generic typical day hourly profiles dependent on temperature and country for space heating demand in the tertiary sector




## Repository structure
Files:
```
data/hotmaps_task_2.7_load_profile_tertiary_heating_generic.csv              -- contains the dataset in CSV format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```

## Documentation


This dataset provides the hourly heat demand for space heating in the tertiary sector for typical days with different temperature levels for the EU-27, UK+CH+LI. 

The profiles can be used to assemble a yearlong demand profile for a NUTS2 region, if the daily average temperature for the specific region and year is available. 

Create your own profile.
For heating and cooling, we provided a yearlong profile for the year 2010. However, we want to give the user the opportunity to use a year of his/her choice. Additionally, if users have access to location-specific hourly temperature profiles, we want to give the user the opportunity to use this data in order to generation load profiles with a higher precision.
Therefore, the generic profiles are supposed to enable the user to produce load profiles of his/her own using own data and a structure year of her/his own choice. 
Structure year in this context means the order of days in the course of the year. In contrast to demand for sanitary hot water, we assume that demand for heating and cooling depends on the typeday, the hour of the day itself and the outside temperature in the respective hour.
The profiles provided here are unitless, since they must be scaled during the generation of yearlong profiles. For the generic profiles for heating and cooling, they are driven by the differences between hours and temperature levels. Additionally, since the tertiary sector is driven by a weekly rhythm, the profiles for heating and cooling in the tertiary sector depend also on the day type. The column “day type” refers to the type of a day in the week:
- weekdays = typeday 0;
- saturday or day before a holiday = typeday 1;
- sunday or holiday = typeday 2
The tertiary sector profile consists of demand from multiple subsectors. The configuration is different for each country. For the respective subsectoral shares per country we refer to the hotmaps WP2 report, section 2.7.3 (https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf). As CH + LI are not part of the EU, they are not specifically modeled. We, thus, used the generic profiles of AT for both countries. 

Yearlong profiles can be generated from the generic profiles for tertiary heating and cooling provided in this repository following the following steps:
- determining the structure year for which the profiles are generated
- choosing the correct combination of day type, hour of the day, temperature and demand from the generic profile for each hour of the year in order to get a yearlong, unitless profile
- scaling the total sum of the annual yearlong profile (i.e. the integral of the profile) according to the annual total demand
- please mind: to minimize data space, the dataset is cut above 17° Celcius. We assume that the heating load for 18° Celsius or higher is 0.




For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.8 page 121ff.

## Limitations of the datasets

The datasets provided have to be interpreted as simplified indicator to enable the analysis that can be carried out within the Hotmaps toolbox. It should be noted that the results of the toolbox can be improved with recorded heat demand data of local representatives of the tertiary sector.

## References
[1] [Harmonised European time use surveys](http://ec.europa.eu/eurostat/web/products-manuals-and-guidelines/-/KS-RA-08-014) Eurostat, 2009, checked on 2/1/2018.

[2] [Synthetic load profile heat pump](https://www.swm-infrastruktur.de/strom/netzzugang/bedingungen/waermepumpe.html)(https://www.trust-ee.eu/files/otherfiles/0000/0008/TrustEE_D1_1.pdf) Munich City Utilities, 2012, checked on 5/10/2018.

## How to cite


Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) 


## Authors

Matthias Kuehnbach, Simon Marwitz, Anna-Lena Klingler <sup>*</sup>,

<sup>*</sup> [Fraunhofer ISI](https://isi.fraunhofer.de/)
Fraunhofer ISI, Breslauer Str. 48, 
76139 Karlsruhe


## License


Copyright © 2016-2018: Matthias Kuehnbach, Anna-Lena Klingler, Simon Marwitz

Creative Commons Attribution 4.0 International License

This work is licensed under a Creative Commons CC BY 4.0 International License.


SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.
